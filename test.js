const { readFromCSV, updateCSV, main, generateUpdatedRecord, compareDateStrings } = require('./app');




test('Task proceeds without errors ------------------------------------------- (func: main) ------------------------->', async() => {
    expect(await main()).toBe(1);
});


test('12.02.2019 = 12.02.2019 ------------------------------------------------ (func: compareDateStrings) ----------->', () => {
    expect(compareDateStrings('12.02.2019','12.02.2019')).toBe(0);
});

test('26.11.2020 > 12.02.2019 ------------------------------------------------ (func: compareDateStrings) ----------->', () => {
    expect(compareDateStrings('26.11.2020','12.02.2019')).toBe(1);
});

test('26.11.2010 < 12.02.2019 ------------------------------------------------ (func: compareDateStrings) ----------->', () => {
    expect(compareDateStrings('26.11.2010','12.02.2019')).toBe(-1);
});






test('Reads Data Correctly from testRead.csv --------------------------------- (func: readFromCSV) ------------------>', async() => {
   expect((await readFromCSV('./testRead.csv'))).toStrictEqual({
       "Georgia": ["Georgia", "02.03.2020", "text about georgia", "closed", "1"], 
       "US": ["US", "02.03.2012", "text about us", "suspended", "2"]});
});


test('Writes Data in empty testRead.csv -------------------------------------- (func: updateCSV) -------------------->', async() => {
    await updateCSV('./testWrite.csv', [['testCoutry','10.10.2010','test text','test status','3']])
    const read = await readFromCSV('./testWrite.csv');
    expect(read).toStrictEqual({
        testCoutry: [ 'testCoutry', '10.10.2010', 'test text', 'test status', '3' ]
    });
});


test('Updates Data in testRead.csv ------------------------------------------- (func: updateCSV) -------------------->', async() => {
    await updateCSV('./testWrite.csv', [['testCoutry','10.10.2010','test text','test status','3']])
    await updateCSV('./testWrite.csv', [['updatedCoutry','10.10.2010','updated text','updated status','7']])
    const read = await readFromCSV('./testWrite.csv');
    expect(read).toStrictEqual({
        updatedCoutry: [ 'updatedCoutry','10.10.2010','updated text','updated status','7' ]
      });
});

    
test('Generates Updated Record Compared To testGenerateNewRecord.csv --------- (func: generateUpdatedRecord) -------->', async() => {
    const record = ['ARGENTINA</b>, -- published 07.05.2020 \n' +
    '1. Passengers are not allowed to enter Argentina.\n' +
    '- This does not apply to nationals and residents of Argentina.\n' +
    "2. Airlines operating repatriation flights must send passenger's information to ANAC 12 hours before departure. This information must contain passenger list with travel document numbers, phone number and address where each person declares that they will comply with the mandatory quarantine upon arrival.\n"]
    
    expect(await generateUpdatedRecord('./testGenerateNewRecord.csv', record)).toStrictEqual([
        [   'ARGENTINA',' 07.05.2020 ',
            '1. Passengers are not allowed to enter Argentina.\n' +
            '- This does not apply to nationals and residents of Argentina.\n' +
            "2. Airlines operating repatriation flights must send passenger's information to ANAC 12 hours before departure. This information must contain passenger list with travel document numbers, phone number and address where each person declares that they will comply with the mandatory quarantine upon arrival.\n",
            'n/a',
            1
        ]
      ]);
});