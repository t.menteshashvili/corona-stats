const axios = require('axios')
const { parse } = require('node-html-parser')
const createCsvWriter = require('csv-writer').createArrayCsvWriter;
const csv = require('csv-parser')
const fs = require('fs')



// fetches html of website and generates the array of strings containing coresponding paragraphs of data
 const fetchAndGenerateCountryBlocks = async() =>{
    axios.defaults.adapter = require('axios/lib/adapters/http')
    const res = await axios.get('https://www.iatatravelcentre.com/international-travel-document-news/1580226297.htm');

    const countryStringBlocks = parse(res.data).querySelector('.middle').childNodes[9].childNodes.slice(10)
                                                                                .toString('')
                                                                                .split('</b>,<span>').join('')
                                                                                .split('<p>').join('')
                                                                                .split(',<br>,').join('\n')
                                                                                .split('&#32;').join(' ')
                                                                                .split('<br>').join('\n')
                                                                                .split('<b> </b>').join('')
                                                                                .split(',<b>').join('<b>')
                                                                                .split('<b>');
    countryStringBlocks.shift();
    // console.log(countryStringBlocks)
    return countryStringBlocks;
    
}


// gets new record for each country and generates updated record, if the date is newer than existing 'Last Update' field in result.csv for this specific country
 const generateUpdatedRecord = async(path,countryBlocks) => {
    const result = [];

    const previousrecord = await readFromCSV(path);

    countryBlocks.map(countryBlock=>{
        const countryLines = countryBlock.split('\n');
        const [country,,,,,,date] = countryLines[0].split(/<(\/)b>(,){0,1}( ){0,3}(-){0,4}( )*published/);
        const countryName = country.trim();
        const previousCountryRow = previousrecord[countryName];

        let version = 1;

        if(previousCountryRow){
            delete previousrecord[countryName];
            if(compareDateStrings(previousCountryRow[1],date)<0){
                result.push(previousCountryRow);
                return;
            }else{
                version = Number(previousCountryRow[4])+1;
            }
        }

        const status = countryLines[1] && ( countryLines[1].includes('closed') ? 'closed' : 
                                            countryLines[1].includes('suspended') ? 'suspended' :
                                            'n/a' ) || 'n/a';
        const text =  countryLines.splice(status === 'n/a' ? 1 : 2).join('\n')
        result.push([countryName, date, text, status, version])
    })
    const restFromPrevious = Object.keys(previousrecord).map(cntry=>{if(cntry&&cntry!='undefined'){return previousrecord[cntry]}})
    // console.log([restFromPrevious])
    return [...result, ...restFromPrevious];
}


// writes updated record in result.csv
 const updateCSV = async(path,record) => {
    const csvWriter = createCsvWriter({
        header: ['Country', 'Last Updated', 'Text', 'Status', 'Version'],
        path
    });
    await new Promise((resolve, reject)=>{
        csvWriter.writeRecords(record)     
        .then(() => {
            console.log('...Done');
            resolve();
        });
    });
}



// reads the csv file and returns the object with properties named after countries and values of type array containing corresponding row
 const readFromCSV = async(path) => {
    const result = {};

    await new Promise((resolve, reject)=>{
        fs.createReadStream(path)
        .pipe(csv())
        .on('data',(data) => {
            const {Country, Text, Status, Version} = data;
            result[Country] = [Country, data['Last Updated'], Text, Status, Version];
        })
        .on('end', () => {
            console.log('Finished Reading From result.csv');
            resolve();
        });
    }) 

    return result;
}



// generates 8digit integer of structure 'YYYYMMDD' to simply compare dates of type string
const compareDateStrings = (date1, date2) => {
    if(!date1 || !date2){return -2}
    const comparable1 = date1.trim().split('.').join('-').split('-').reverse().join('')
    const comparable2 = date2.trim().split('.').join('-').split('-').reverse().join('')
    return (comparable1 - comparable2) > 0 ? 1 : (comparable1 - comparable2) < 0 ? -1 : 0;
}




const main = async() => {
    try{
        fs.open('./result.csv', 'wx', ()=>{return});
        const fetchedData = await fetchAndGenerateCountryBlocks();
        const updatedRecord = await generateUpdatedRecord('./result.csv',fetchedData);
        await updateCSV('./result.csv', updatedRecord)
        return 1
    }catch(err){
        console.log('Error:   ',err.message)
    }
}

main();



module.exports={
    fetchAndGenerateCountryBlocks,
    generateUpdatedRecord,
    updateCSV,
    readFromCSV,
    compareDateStrings,
    main
}



